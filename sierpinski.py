#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Dywan Sierpińskiego.

Parametry:
    bok: liczba rzeczywista dodatnia
    poziom: liczba całkowita >= 0
    żółw: obiekt klasy Żółw
'''

from zolw import Żółw


def sierpiński(bok, poziom, żółw):
    if poziom == 0:
        for i in range(0, 3):
            żółw.naprzód(bok)
            żółw.w_lewo(120)
    else:
        sierpiński(bok / 2, poziom - 1, żółw)
        żółw.naprzód(bok / 2)
        sierpiński(bok/2, poziom - 1, żółw)
        żółw.wstecz(bok / 2)
        żółw.w_lewo(60)
        żółw.naprzód(bok / 2)
        żółw.w_prawo(60)
        sierpiński(bok / 2, poziom - 1, żółw)
        żółw.w_lewo(60)
        żółw.wstecz(bok / 2)
        żółw.w_prawo(60)


if __name__ == '__main__':
    tolek = Żółw()
    sierpiński(1, 6, tolek)
    tolek.rysuj()
