#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Segment:
    '''Zapis położeń żółwia, podczas których
    stan pisaka nie uległ zmianie.
    '''
    
    def __init__(self, x, y, pisak, **styl):
        self.X = [x]
        self.Y = [y]
        self.pisak = pisak
        self.styl = styl

    def dodaj_pozycję(self, x, y):
        self.X.append(x)
        self.Y.append(y)
