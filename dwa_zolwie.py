#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import cycle
from zolw import Żółw


tolek = Żółw()
kolory = cycle('black gray brown'.split())

for _ in range(20):
    kolor = next(kolory)
    tolek.kolor_pisaka(kolor)
    tolek.naprzód(1)
    tolek.w_lewo(100)

tolek.podnieś_pisak()
tolek.przestaw(3, 3)
tolek.opuść_pisak()

for _ in range(20):
    kolor = next(kolory)
    tolek.kolor_pisaka(kolor)
    tolek.naprzód(1)
    tolek.w_lewo(100)

franklin = Żółw(x=0, y=3)
kolory = cycle('yellow purple magenta red'.split())

for _ in range(20):
    kolor = next(kolory)
    franklin.kolor_pisaka(kolor)
    franklin.naprzód(1)
    franklin.w_lewo(100)

tolek.rysuj(pokaż=False)
franklin.rysuj()
