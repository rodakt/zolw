#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Krzywa Hilberta.

Parametry:
    poziom: liczba całkowita >= 0
    kąt: liczba rzeczywista
    bok: liczba rzeczywista dodatnia
    żółw: obiekt klasy Żółw
'''

from zolw import Żółw


def hilbert(poziom, kąt, bok, żółw):

    if poziom == 0:
        return

    żółw.w_prawo(kąt)
    hilbert(poziom - 1, -kąt, bok, żółw)

    żółw.naprzód(bok)
    żółw.w_lewo(kąt)
    hilbert(poziom - 1, kąt, bok, żółw)

    żółw.naprzód(bok)
    hilbert(poziom - 1, kąt, bok, żółw)

    żółw.w_lewo(kąt)
    żółw.naprzód(bok)
    hilbert(poziom - 1, -kąt, bok, żółw)
    żółw.w_prawo(kąt)


if __name__ == '__main__':
    tolek = Żółw()
    hilbert(6, 80, 1, tolek)
    tolek.rysuj()
